$(document)
  .ready(function() {
    var mozL10n = document.mozL10n || document.webL10n;
     
    $('.filter.menu .item')
      .tab()
    ;

    $('.ui.menu .item')
      .tab()
    ;

    $('#unread').click(function(){
      $('#page_online').hide();
      $('#page_sd').show();
    });
    $('#saved, .ui.icon.button.buscar_open').click(function(){
      $('#page_online').show();
      $('#page_sd').hide();
    });

    $('.ui.rating')
      .rating({
        clearable: true
      })
    ;

    $('.menu_left.sidebar')
      .sidebar({
        overlay: true
      })
      .sidebar('attach events', '.click_menu')
    ;

    $('.menu_top.sidebar')
      .sidebar({
        overlay: true
      })
      .sidebar('attach events', '.m_top')
    ;

    $('.contacts.sidebar')
      .sidebar({
        overlay: true
      })
      .sidebar('attach events', '.contact')
    ;
    $('.buscar_open').click(function(){
        $('.buscador.sidebar').addClass('active');
        $('#home').removeClass();
        $('.buscador.sidebar').attr('style', 'height:50px !important');
    });
    $('.buscar_close').click(function(){
        $('.buscador.sidebar').removeClass('active');
        $('.buscador.sidebar').attr('style', 'height:40px !important');
    });
      
    $('.ui.dropdown')
      .dropdown()
    ;

    $('.ui.accordion')
      .accordion()
    ;

    $('.help.icon').parent().click(function(){
        $('.ui.celled.grid.content').hide();
        $('.ui.icon.button.buscar_open').parent().hide();
        $('.ui.message.help').show();

        $(".menu_principal").show();
            /*$(".ui.celled.grid.content").show();*/
            $(".ui.celled.grid.viewer").hide();
            $(".ui.celled.grid.viewer iframe").remove()
            $(".ui.menu.fixed_buttom").hide();
            $('.menu_top.sidebar')
              .sidebar('hide')
            ;
            $('.ui.menu_left.sidebar.menu').removeClass('active');
    });
    $('.contact').click(function(){
        $('.ui.menu_left.sidebar.menu').removeClass('active');
    });
    $('.reply.mail.help, .active.home.icon').parent().click(function(){
        $('.ui.celled.grid.content').show();
        $('.ui.icon.button.buscar_open').parent().show();
        $('.ui.message.help').hide();
        $(".ui.menu.fixed_buttom").hide();
        
        $(".menu_principal").show();
            $(".ui.celled.grid.content").show();
            $(".ui.celled.grid.viewer").hide();
            $(".ui.celled.grid.viewer iframe").remove()
            $(".ui.menu.fixed_buttom").hide();
            $('.menu_top.sidebar')
              .sidebar('hide')
            ;
            $('.ui.menu_left.sidebar.menu').removeClass('active');
    });
    $('.ui.modal.email.foxxapp')
      .modal('setting', {
        onShow    : function(){
          var web_construction = mozL10n.get('web_construction', null, 'Sorry, our web page is actually in construction. If you like it, you can register your e-mail to stay pending of our latest news.');
          window.alert(web_construction);
        }
      })
      .modal('attach events', '.ui.image.foxxapp, .ui.button.signup', 'show')
    ;
    $('.ui.modal.email.foxxapp')
      .parent().css({"margin":"0"})
    ;
  })
;