$(document).ready(function(){
    var mozL10n = document.mozL10n || document.webL10n;
    var msg_traduction = mozL10n.get('msg_traduction', null, 'Sorry we are making the translation to the Portuguese language, we regret the inconvenience while we offer the Spanish and English language.');
    
    var locale = navigator.language;
    $('.item.lang.'+locale+'').css({"background-color": "rgba(0,0,0,.03)", "-webkit-box-shadow": "none", "box-shadow": "none", "border-color": "#D95C5C", "color": "#D95C5C"});
    mozL10n.setLanguage(locale);
    $('.item.lang.en-US').click(function(){
        $('.item.lang').removeAttr('style');
        $('.item.lang').removeClass('active');
        $('.item.lang.en-US').addClass('active');
        $('.ui.menu_left.sidebar.menu').removeClass('active');
        mozL10n.setLanguage('en-US');
        $('#email').attr('placeholder',"E-mail");
        $('#search').attr('placeholder',"Enter a keyword");
    });
    $('.item.lang.es').click(function(){
        $('.item.lang').removeAttr('style');
        $('.item.lang').removeClass('active');
        $('.item.lang.es').addClass('active');
        $('.ui.menu_left.sidebar.menu').removeClass('active');
        mozL10n.setLanguage('es');
        $('#email').attr('placeholder',"Correo electrónico");
        $('#search').attr('placeholder',"Intro, Palabra clave:");
    });
    $('.item.lang.pl').click(function(){
        alert(msg_traduction);
        $('.item.lang').removeAttr('style');
        $('.item.lang').removeClass('active');
        $('.item.lang.en-US').addClass('active');
        $('.ui.menu_left.sidebar.menu').removeClass('active');
        mozL10n.setLanguage('en-US');
        $('#email').attr('placeholder',"E-mail");
        $('#search').attr('placeholder',"Enter a keyword.");
    });
});
